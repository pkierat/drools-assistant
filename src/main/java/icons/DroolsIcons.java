package icons;

import com.intellij.ui.IconManager;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public final class DroolsIcons {
  private static @NotNull Icon load(@NotNull String path, int cacheKey, int flags) {
  return IconManager.getInstance().loadRasterizedIcon(path, DroolsIcons.class.getClassLoader(), cacheKey, flags);
}
  /** 16x16 */ public static final @NotNull Icon DRL = load("dev/kierat/drools/icons/drl.svg", 40277942, 0);
}

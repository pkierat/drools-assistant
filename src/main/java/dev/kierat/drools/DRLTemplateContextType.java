package dev.kierat.drools;

import com.intellij.codeInsight.template.TemplateContextType;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.psi.PsiFile;
import dev.kierat.drools.psi.DRLFile;
import dev.kierat.drools.psi.DRLFileType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class DRLTemplateContextType extends TemplateContextType {
  @NonNls
  private static final String CONTEXT_NAME = "DRL_FILE";

  public DRLTemplateContextType() {
    super(CONTEXT_NAME, DroolsBundle.message("live.templates.context.drools.name"));
  }

  @Override
  public boolean isInContext(@NotNull final PsiFile file, final int offset) {
    return file instanceof DRLFile;
  }

  @Override
  public SyntaxHighlighter createHighlighter() {
    return new DRLSyntaxHighlighter(DRLFileType.INSTANCE);
  }
}


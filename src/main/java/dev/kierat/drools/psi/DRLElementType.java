package dev.kierat.drools.psi;

import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;


public class DRLElementType extends IElementType {
  public DRLElementType(@NotNull @NonNls String debugName) {
    super(debugName, DRLLanguage.INSTANCE);
  }
}

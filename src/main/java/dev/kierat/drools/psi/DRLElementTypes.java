package dev.kierat.drools.psi;

import com.intellij.lang.ASTFactory;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PlainTextTokenTypes;
import com.intellij.psi.tree.IFileElementType;
import org.jetbrains.annotations.NotNull;

public interface DRLElementTypes {
    IFileElementType DRL_FILE = new IFileElementType(DRLLanguage.INSTANCE);

    DRLElementType PACKAGE = new DRLElementType("package");
    DRLElementType IMPORT = new DRLElementType("import");
    DRLElementType UNIT = new DRLElementType("unit");
    DRLElementType DIALECT = new DRLElementType("dialect");
    DRLElementType DECLARATION = new DRLElementType("DECLARATION");
    DRLElementType RULE = new DRLElementType("RULE");
    DRLElementType FUNCTION = new DRLElementType("FUNCTION");
    DRLElementType QUERY = new DRLElementType("QUERY");

    DRLElementType PACKAGE_NAME = new DRLElementType("package name");

}

package dev.kierat.drools.psi;

import com.intellij.ide.highlighter.custom.SyntaxTable;
import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.fileTypes.impl.CustomSyntaxTableFileType;
import dev.kierat.drools.DroolsBundle;
import icons.DroolsIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;


public class DRLFileType extends LanguageFileType implements CustomSyntaxTableFileType {

  public static final DRLFileType INSTANCE = new DRLFileType();

  private DRLFileType() {
    super(DRLLanguage.INSTANCE);
  }

  @Override
  @NotNull
  public String getName() {
    return "DRL";
  }

  @Override
  @NotNull
  public String getDescription() {
    return DroolsBundle.message("filetype.drl.description");
  }

  @Override
  @NotNull
  public String getDefaultExtension() {
    return "drl";
  }

  @Override
  public Icon getIcon() {
    return DroolsIcons.DRL;
  }

  @Override
  public @NotNull SyntaxTable getSyntaxTable() {
    return table;
  }

  private static final SyntaxTable table;

  private static final String[] KEYWORDS_1 = {
      "acc", "accumulate", "action", "and", "attributes", "boolean", "byte", "calendars",
      "char", "class", "collect", "contains", "declare", "dialect", "direct", "double",
      "end", "entry-point", "eval", "excludes", "exists", "extends", "float", "focus", "forall",
      "from", "function", "global", "group", "import", "in", "init", "instanceof", "int",
      "long", "matches", "memberof", "new", "not", "or", "over", "package", "query", "retract",
      "result", "reverse", "rule", "short", "soundslike", "super", "template", "then", "this",
      "timer", "unit", "void", "when"
  };
  private static final String[] KEYWORDS_2 = {
      "no-loop", "lock-on-active", "salience", "agenda-group", "auto-focus", "ruleflow-group",
      "activation-group", "dialect", "date-effective", "date-expires", "enabled", "duration"
  };
  private static final String[] KEYWORDS_3 = {
      "assert", "break", "case", "catch", "continue", "default", "do", "else", "final", "finally",
      "for", "if", "modify", "return", "static", "switch", "synchronized", "throw", "try", "while",
  };
  private static final String[] KEYWORDS_4 = {
      "abstract", "enum", "implements", "interface", "native", "private", "protected", "public",
      "strictfp", "throws", "trait", "transient", "type", "volatile", "window",
  };

  static {
    table = new SyntaxTable();
    table.setStartComment("/*");
    table.setEndComment("*/");
    table.setLineComment("//");
    table.setHasBraces(true);
    table.setHasBrackets(true);
    table.setHasParens(true);
    table.setIgnoreCase(false);
    table.setHasStringEscapes(true);
    table.setHexPrefix("0x");
    table.setNumPostfixChars("lLIfFdDB");
    for (var kw : KEYWORDS_1) { table.addKeyword1(kw); }
    for (var kw : KEYWORDS_2) { table.addKeyword2(kw); }
    for (var kw : KEYWORDS_3) { table.addKeyword3(kw); }
    for (var kw : KEYWORDS_4) { table.addKeyword4(kw); }
  }

}

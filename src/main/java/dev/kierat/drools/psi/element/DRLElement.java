package dev.kierat.drools.psi.element;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import org.jetbrains.annotations.NotNull;

public class DRLElement extends ASTWrapperPsiElement {

    public DRLElement(@NotNull ASTNode node) {
        super(node);
    }

}

package dev.kierat.drools.psi;

public interface DRLTokenTypes {
    DRLElementType SINGLE_LINE_COMMENT = new DRLElementType("SINGLE_LINE_COMMENT");
    DRLElementType MULTI_LINE_COMMENT_START = new DRLElementType("MULTI_LINE_COMMENT_START");
    DRLElementType MULTI_LINE_COMMENT_END = new DRLElementType("MULTI_LINE_COMMENT_END");

    DRLElementType SINGLE_QUOTED_STRING = new DRLElementType("SINGLE_QUOTED_STRING");
    DRLElementType DOUBLE_QUOTED_STRING = new DRLElementType("DOUBLE_QUOTED_STRING");
    DRLElementType NUMBER = new DRLElementType("NUMBER");
    DRLElementType IDENTIFIER = new DRLElementType("IDENTIFIER");
    DRLElementType WHITE_SPACE = new DRLElementType("WHITE_SPACE");
    DRLElementType DIV = new DRLElementType("DIV");
    DRLElementType MUL = new DRLElementType("MUL");
    DRLElementType DOT = new DRLElementType("DOT");
    DRLElementType PLUS = new DRLElementType("PLUS");
    DRLElementType LBRACE = new DRLElementType("LBRACE");
    DRLElementType RBRACE = new DRLElementType("RBRACE");
    DRLElementType BACKQUOTE = new DRLElementType("BACKQUOTE");
    DRLElementType MINUS = new DRLElementType("MINUS");
    DRLElementType LBRACKET = new DRLElementType("LBRACKET");
    DRLElementType RBRACKET = new DRLElementType("RBRACKET");
    DRLElementType LPAREN = new DRLElementType("LPAREN");
    DRLElementType RPAREN = new DRLElementType("RPAREN");
    DRLElementType SEMICOLON = new DRLElementType("SEMICOLON");
    DRLElementType EQ = new DRLElementType("EQ");
    DRLElementType BIN_NOT = new DRLElementType("BIN_NOT");
    DRLElementType REM = new DRLElementType("REM");
    DRLElementType NOT = new DRLElementType("NOT");
    DRLElementType COMMA = new DRLElementType("COMMA");
    DRLElementType COLON = new DRLElementType("COLON");
    DRLElementType GT = new DRLElementType("GT");
    DRLElementType LT = new DRLElementType("LT");
    DRLElementType OR = new DRLElementType("OR");
    DRLElementType XOR = new DRLElementType("XOR");
    DRLElementType AND = new DRLElementType("AND");
    DRLElementType AT = new DRLElementType("AT");
    DRLElementType HASH = new DRLElementType("HASH");
    DRLElementType DIV_EQ = new DRLElementType("DIV_EQ");
    DRLElementType MUL_EQ = new DRLElementType("MUL_EQ");
    DRLElementType PLUS_PLUS = new DRLElementType("PLUS_PLUS");
    DRLElementType PLUS_EQ = new DRLElementType("PLUS_EQ");
    DRLElementType IF = new DRLElementType("IF");
    DRLElementType IN = new DRLElementType("IN");
    DRLElementType DO = new DRLElementType("DO");
    DRLElementType MINUS_MINUS = new DRLElementType("MINUS_MINUS");
    DRLElementType MINUS_EQ = new DRLElementType("MINUS_EQ");
    DRLElementType EQ_EQ = new DRLElementType("EQ_EQ");
    DRLElementType INT_DIV = new DRLElementType("INT_DIV");
    DRLElementType REM_EQ = new DRLElementType("REM_EQ");
    DRLElementType EXCL_DOT = new DRLElementType("EXCL_DOT");
    DRLElementType NEQ = new DRLElementType("NEQ");
    DRLElementType GT_EQ = new DRLElementType("GT_EQ");
    DRLElementType GT_GT = new DRLElementType("GT_GT");
    DRLElementType LT_EQ = new DRLElementType("LT_EQ");
    DRLElementType LT_LT = new DRLElementType("LT_LT");
    DRLElementType OR_EQ = new DRLElementType("OR_EQ");
    DRLElementType OR_OR = new DRLElementType("OR_OR");
    DRLElementType XOR_EQ = new DRLElementType("XOR_EQ");
    DRLElementType AND_EQ = new DRLElementType("AND_EQ");
    DRLElementType AND_AND = new DRLElementType("AND_AND");
    DRLElementType FOR = new DRLElementType("FOR");
    DRLElementType DOT_DOT_DOT = new DRLElementType("DOT_DOT_DOT");
    DRLElementType END = new DRLElementType("END");
    DRLElementType ACCUMULATE = new DRLElementType("ACCUMULATE");
    DRLElementType TRY = new DRLElementType("TRY");
    DRLElementType INT = new DRLElementType("INT");
    DRLElementType NEW = new DRLElementType("NEW");
    DRLElementType INT_DIV_EQ = new DRLElementType("INT_DIV_EQ");
    DRLElementType GT_GT_EQ = new DRLElementType("GT_GT_EQ");
    DRLElementType LT_LT_EQ = new DRLElementType("LT_LT_EQ");
    DRLElementType OR_OR_EQ = new DRLElementType("OR_OR_EQ");
    DRLElementType AND_AND_EQ = new DRLElementType("AND_AND_EQ");
    DRLElementType FROM = new DRLElementType("FROM");
    DRLElementType ELSE = new DRLElementType("ELSE");
    DRLElementType ENUM = new DRLElementType("ENUM");
    DRLElementType EVAL = new DRLElementType("EVAL");
    DRLElementType CASE = new DRLElementType("CASE");
    DRLElementType CHAR = new DRLElementType("CHAR");
    DRLElementType UNIT = new DRLElementType("UNIT");
    DRLElementType LONG = new DRLElementType("LONG");
    DRLElementType TYPE = new DRLElementType("TYPE");
    DRLElementType THEN = new DRLElementType("THEN");
    DRLElementType THIS = new DRLElementType("THIS");
    DRLElementType INIT = new DRLElementType("INIT");
    DRLElementType OVER = new DRLElementType("OVER");
    DRLElementType RULE = new DRLElementType("RULE");
    DRLElementType BYTE = new DRLElementType("BYTE");
    DRLElementType VOID = new DRLElementType("VOID");
    DRLElementType WHEN = new DRLElementType("WHEN");
    DRLElementType FLOAT = new DRLElementType("FLOAT");
    DRLElementType FINAL = new DRLElementType("FINAL");
    DRLElementType FOCUS = new DRLElementType("FOCUS");
    DRLElementType CATCH = new DRLElementType("CATCH");
    DRLElementType CLASS = new DRLElementType("CLASS");
    DRLElementType TIMER = new DRLElementType("TIMER");
    DRLElementType TRAIT = new DRLElementType("TRAIT");
    DRLElementType THROW = new DRLElementType("THROW");
    DRLElementType BREAK = new DRLElementType("BREAK");
    DRLElementType SUPER = new DRLElementType("SUPER");
    DRLElementType SHORT = new DRLElementType("SHORT");
    DRLElementType GROUP = new DRLElementType("GROUP");
    DRLElementType WHILE = new DRLElementType("WHILE");
    DRLElementType QUERY = new DRLElementType("QUERY");
    DRLElementType FORALL = new DRLElementType("FORALL");
    DRLElementType EXISTS = new DRLElementType("EXISTS");
    DRLElementType ACTION = new DRLElementType("ACTION");
    DRLElementType ASSERT = new DRLElementType("ASSERT");
    DRLElementType MODIFY = new DRLElementType("MODIFY");
    DRLElementType THROWS = new DRLElementType("THROWS");
    DRLElementType IMPORT = new DRLElementType("IMPORT");
    DRLElementType NATIVE = new DRLElementType("NATIVE");
    DRLElementType DIRECT = new DRLElementType("DIRECT");
    DRLElementType DOUBLE = new DRLElementType("DOUBLE");
    DRLElementType RETURN = new DRLElementType("RETURN");
    DRLElementType RESULT = new DRLElementType("RESULT");
    DRLElementType STATIC = new DRLElementType("STATIC");
    DRLElementType SWITCH = new DRLElementType("SWITCH");
    DRLElementType PUBLIC = new DRLElementType("PUBLIC");
    DRLElementType GLOBAL = new DRLElementType("GLOBAL");
    DRLElementType WINDOW = new DRLElementType("WINDOW");
    DRLElementType FINALLY = new DRLElementType("FINALLY");
    DRLElementType EXTENDS = new DRLElementType("EXTENDS");
    DRLElementType ENABLED = new DRLElementType("ENABLED");
    DRLElementType COLLECT = new DRLElementType("COLLECT");
    DRLElementType MATCHES = new DRLElementType("MATCHES");
    DRLElementType NO_LOOP = new DRLElementType("NO_LOOP");
    DRLElementType DEFAULT = new DRLElementType("DEFAULT");
    DRLElementType DECLARE = new DRLElementType("DECLARE");
    DRLElementType DIALECT = new DRLElementType("DIALECT");
    DRLElementType RETRACT = new DRLElementType("RETRACT");
    DRLElementType REVERSE = new DRLElementType("REVERSE");
    DRLElementType BOOLEAN = new DRLElementType("BOOLEAN");
    DRLElementType PACKAGE = new DRLElementType("PACKAGE");
    DRLElementType PRIVATE = new DRLElementType("PRIVATE");
    DRLElementType FUNCTION = new DRLElementType("FUNCTION");
    DRLElementType EXCLUDES = new DRLElementType("EXCLUDES");
    DRLElementType ABSTRACT = new DRLElementType("ABSTRACT");
    DRLElementType CONTAINS = new DRLElementType("CONTAINS");
    DRLElementType CONTINUE = new DRLElementType("CONTINUE");
    DRLElementType MEMBEROF = new DRLElementType("MEMBEROF");
    DRLElementType TEMPLATE = new DRLElementType("TEMPLATE");
    DRLElementType DURATION = new DRLElementType("DURATION");
    DRLElementType SALIENCE = new DRLElementType("SALIENCE");
    DRLElementType STRICTFP = new DRLElementType("STRICTFP");
    DRLElementType VOLATILE = new DRLElementType("VOLATILE");
    DRLElementType CALENDARS = new DRLElementType("CALENDARS");
    DRLElementType TRANSIENT = new DRLElementType("TRANSIENT");
    DRLElementType INTERFACE = new DRLElementType("INTERFACE");
    DRLElementType PROTECTED = new DRLElementType("PROTECTED");
    DRLElementType AUTO_FOCUS = new DRLElementType("AUTO_FOCUS");
    DRLElementType ATTRIBUTES = new DRLElementType("ATTRIBUTES");
    DRLElementType IMPLEMENTS = new DRLElementType("IMPLEMENTS");
    DRLElementType INSTANCEOF = new DRLElementType("INSTANCEOF");
    DRLElementType SOUNDSLIKE = new DRLElementType("SOUNDSLIKE");
    DRLElementType ENTRY_POINT = new DRLElementType("ENTRY_POINT");
    DRLElementType AGENDA_GROUP = new DRLElementType("AGENDA_GROUP");
    DRLElementType DATE_EXPIRES = new DRLElementType("DATE_EXPIRES");
    DRLElementType SYNCHRONIZED = new DRLElementType("SYNCHRONIZED");
    DRLElementType LOCK_ON_ACTIVE = new DRLElementType("LOCK_ON_ACTIVE");
    DRLElementType DATE_EFFECTIVE = new DRLElementType("DATE_EFFECTIVE");
    DRLElementType RULEFLOW_GROUP = new DRLElementType("RULEFLOW_GROUP");
    DRLElementType ACTIVATION_GROUP = new DRLElementType("ACTIVATION_GROUP");
    DRLElementType BAD_CHARACTER = new DRLElementType("BAD_CHARACTER");
}
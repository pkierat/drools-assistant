package dev.kierat.drools.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.SearchScope;
import org.jetbrains.annotations.NotNull;


public class DRLFile extends PsiFileBase implements PsiFile {

    public DRLFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, DRLLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return DRLFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "DRL File";
    }

    @NotNull
    @Override
    public SearchScope getUseScope() {
        return GlobalSearchScope.allScope(getProject());
    }

}

package dev.kierat.drools.psi;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import static dev.kierat.drools.psi.DRLTokenTypes.*;

%%

%class DRLLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{
  return;
%eof}

DIGIT=[0-9]
HEX_DIGIT=[0-9a-fA-F]
LETTER=[a-z]|[A-Z]
WHITE_SPACE=[ \n\t\f]+
SINGLE_LINE_COMMENT="/""/"[^\n]*

MULTI_LINE_COMMENT_START      = "/*"
MULTI_LINE_COMMENT_END        = "*/"

SINGLE_QUOTED_STRING= (\' ([^\'\n])* \'?)
DOUBLE_QUOTED_STRING= (\" ([^\"\n])* \"?)

IDENTIFIER_START_NO_DOLLAR={LETTER}|"_"
IDENTIFIER_START={IDENTIFIER_START_NO_DOLLAR}|"$"
IDENTIFIER_PART_NO_DOLLAR={IDENTIFIER_START_NO_DOLLAR}|{DIGIT}
IDENTIFIER_PART={IDENTIFIER_START}|{DIGIT}
IDENTIFIER={IDENTIFIER_START}{IDENTIFIER_PART}*
IDENTIFIER_NO_DOLLAR={IDENTIFIER_START_NO_DOLLAR}{IDENTIFIER_PART_NO_DOLLAR}*

NUMERIC_LITERAL = {NUMBER} | {HEX_NUMBER}
NUMBER = ({DIGIT}+ ("." {DIGIT}+)? {EXPONENT}?) | ("." {DIGIT}+ {EXPONENT}?)
EXPONENT = [Ee] ["+""-"]? {DIGIT}*
HEX_NUMBER = 0 [Xx] {HEX_DIGIT}*

%%

<YYINITIAL> "{"                             { return LBRACE; }
<YYINITIAL> "}"                             { return RBRACE; }
<YYINITIAL> "`"                             { return BACKQUOTE; }

<YYINITIAL> {WHITE_SPACE}                   { return WHITE_SPACE;             }

// comments
<YYINITIAL> {SINGLE_LINE_COMMENT}           { return SINGLE_LINE_COMMENT;     }
<YYINITIAL> {MULTI_LINE_COMMENT_START}      { return MULTI_LINE_COMMENT_START; }
<YYINITIAL> {MULTI_LINE_COMMENT_END}        { return MULTI_LINE_COMMENT_END; }

// reserved words
<YYINITIAL> "acc"                           { return ACCUMULATE; }
<YYINITIAL> "accumulate"                    { return ACCUMULATE; }
<YYINITIAL> "action"                        { return ACTION; }
<YYINITIAL> "and"                           { return AND; }
<YYINITIAL> "attributes"                    { return ATTRIBUTES; }
<YYINITIAL> "boolean"                       { return BOOLEAN; }
<YYINITIAL> "byte"                          { return BYTE; }
<YYINITIAL> "calendars"                     { return CALENDARS; }
<YYINITIAL> "char"                          { return CHAR; }
<YYINITIAL> "class"                         { return CLASS; }
<YYINITIAL> "collect"                       { return COLLECT; }
<YYINITIAL> "contains"                      { return CONTAINS; }
<YYINITIAL> "declare"                       { return DECLARE; }
<YYINITIAL> "dialect"                       { return DIALECT; }
<YYINITIAL> "direct"                        { return DIRECT; }
<YYINITIAL> "double"                        { return DOUBLE; }
<YYINITIAL> "end"                           { return END; }
<YYINITIAL> "entry-point"                   { return ENTRY_POINT; }
<YYINITIAL> "eval"                          { return EVAL; }
<YYINITIAL> "excludes"                      { return EXCLUDES; }
<YYINITIAL> "exists"                        { return EXISTS; }
<YYINITIAL> "extends"                       { return EXTENDS; }
<YYINITIAL> "float"                         { return FLOAT; }
<YYINITIAL> "focus"                         { return FOCUS; }
<YYINITIAL> "forall"                        { return FORALL; }
<YYINITIAL> "from"                          { return FROM; }
<YYINITIAL> "function"                      { return FUNCTION; }
<YYINITIAL> "global"                        { return GLOBAL; }
<YYINITIAL> "group"                         { return GROUP; }
<YYINITIAL> "import"                        { return IMPORT; }
<YYINITIAL> "in"                            { return IN; }
<YYINITIAL> "init"                          { return INIT; }
<YYINITIAL> "instanceof"                    { return INSTANCEOF; }
<YYINITIAL> "int"                           { return INT; }
<YYINITIAL> "long"                          { return LONG; }
<YYINITIAL> "matches"                       { return MATCHES; }
<YYINITIAL> "memberof"                      { return MEMBEROF; }
<YYINITIAL> "new"                           { return NEW; }
<YYINITIAL> "not"                           { return NOT; }
<YYINITIAL> "or"                            { return OR; }
<YYINITIAL> "over"                          { return OVER; }
<YYINITIAL> "package"                       { return PACKAGE; }
<YYINITIAL> "query"                         { return QUERY; }
<YYINITIAL> "retract"                       { return RETRACT; }
<YYINITIAL> "result"                        { return RESULT; }
<YYINITIAL> "reverse"                       { return REVERSE; }
<YYINITIAL> "rule"                          { return RULE; }
<YYINITIAL> "short"                         { return SHORT; }
<YYINITIAL> "soundslike"                    { return SOUNDSLIKE; }
<YYINITIAL> "super"                         { return SUPER; }
<YYINITIAL> "template"                      { return TEMPLATE; }
<YYINITIAL> "then"                          { return THEN; }
<YYINITIAL> "this"                          { return THIS; }
<YYINITIAL> "timer"                         { return TIMER; }
<YYINITIAL> "unit"                          { return UNIT; }
<YYINITIAL> "void"                          { return VOID; }
<YYINITIAL> "when"                          { return WHEN; }

// rule attributes
<YYINITIAL> "no-loop"                       { return NO_LOOP; }
<YYINITIAL> "lock-on-active"                { return LOCK_ON_ACTIVE; }
<YYINITIAL> "salience"                      { return SALIENCE; }
<YYINITIAL> "agenda-group"                  { return AGENDA_GROUP; }
<YYINITIAL> "auto-focus"                    { return AUTO_FOCUS; }
<YYINITIAL> "ruleflow-group"                { return RULEFLOW_GROUP; }
<YYINITIAL> "activation-group"              { return ACTIVATION_GROUP; }
<YYINITIAL> "dialect"                       { return DIALECT; }
<YYINITIAL> "date-effective"                { return DATE_EFFECTIVE; }
<YYINITIAL> "date-expires"                  { return DATE_EXPIRES; }
<YYINITIAL> "enabled"                       { return ENABLED; }
<YYINITIAL> "duration"                      { return DURATION; }

<YYINITIAL> "assert"                        { return ASSERT; }
<YYINITIAL> "break"                         { return BREAK; }
<YYINITIAL> "case"                          { return CASE; }
<YYINITIAL> "catch"                         { return CATCH; }
<YYINITIAL> "continue"                      { return CONTINUE; }
<YYINITIAL> "default"                       { return DEFAULT; }
<YYINITIAL> "do"                            { return DO; }
<YYINITIAL> "else"                          { return ELSE; }
<YYINITIAL> "final"                         { return FINAL; }
<YYINITIAL> "finally"                       { return FINALLY; }
<YYINITIAL> "for"                           { return FOR; }
<YYINITIAL> "if"                            { return IF; }
<YYINITIAL> "modify"                        { return MODIFY; }
<YYINITIAL> "return"                        { return RETURN; }
<YYINITIAL> "static"                        { return STATIC; }
<YYINITIAL> "switch"                        { return SWITCH; }
<YYINITIAL> "synchronized"                  { return SYNCHRONIZED; }
<YYINITIAL> "throw"                         { return THROW; }
<YYINITIAL> "try"                           { return TRY; }
<YYINITIAL> "while"                         { return WHILE; }

<YYINITIAL> "abstract"                      { return ABSTRACT; }
<YYINITIAL> "enum"                          { return ENUM; }
<YYINITIAL> "implements"                    { return IMPLEMENTS; }
<YYINITIAL> "interface"                     { return INTERFACE; }
<YYINITIAL> "native"                        { return NATIVE; }
<YYINITIAL> "private"                       { return PRIVATE; }
<YYINITIAL> "protected"                     { return PROTECTED; }
<YYINITIAL> "public"                        { return PUBLIC; }
<YYINITIAL> "strictfp"                      { return STRICTFP; }
<YYINITIAL> "throws"                        { return THROWS; }
<YYINITIAL> "trait"                         { return TRAIT; }
<YYINITIAL> "transient"                     { return TRANSIENT; }
<YYINITIAL> "type"                          { return TYPE; }
<YYINITIAL> "volatile"                      { return VOLATILE; }
<YYINITIAL> "window"                        { return WINDOW; }

<YYINITIAL> {IDENTIFIER}                    { return IDENTIFIER; }
<YYINITIAL> "["                             { return LBRACKET; }
<YYINITIAL> "]"                             { return RBRACKET; }
<YYINITIAL> "("                             { return LPAREN; }
<YYINITIAL> ")"                             { return RPAREN; }
<YYINITIAL> ";"                             { return SEMICOLON; }
<YYINITIAL> "-"                             { return MINUS; }
<YYINITIAL> "-="                            { return MINUS_EQ; }
<YYINITIAL> "--"                            { return MINUS_MINUS; }
<YYINITIAL> "+"                             { return PLUS; }
<YYINITIAL> "++"                            { return PLUS_PLUS; }
<YYINITIAL> "+="                            { return PLUS_EQ; }
<YYINITIAL> "/"                             { return DIV; }
<YYINITIAL> "/="                            { return DIV_EQ; }
<YYINITIAL> "*"                             { return MUL; }
<YYINITIAL> "*="                            { return MUL_EQ; }
<YYINITIAL> "~/"                            { return INT_DIV; }
<YYINITIAL> "~/="                           { return INT_DIV_EQ; }
<YYINITIAL> "%="                            { return REM_EQ; }
<YYINITIAL> "%"                             { return REM; }
<YYINITIAL> "~"                             { return BIN_NOT; }
<YYINITIAL> "!"                             { return NOT; }

<YYINITIAL> "="                             { return EQ; }
<YYINITIAL> "=="                            { return EQ_EQ; }
<YYINITIAL> "!="                            { return NEQ; }
<YYINITIAL> "."                             { return DOT; }
<YYINITIAL> "!."                            { return EXCL_DOT; }
<YYINITIAL> "..."                           { return DOT_DOT_DOT; }
<YYINITIAL> ","                             { return COMMA; }
<YYINITIAL> ":"                             { return COLON; }
<YYINITIAL> ">"                             { return GT; }
<YYINITIAL> ">="                            { return GT_EQ;    }
<YYINITIAL> ">>"                            { return GT_GT;    }
<YYINITIAL> ">>="                           { return GT_GT_EQ; }
<YYINITIAL> "<"                             { return LT; }
<YYINITIAL> "<="                            { return LT_EQ; }
<YYINITIAL> "<<"                            { return LT_LT; }
<YYINITIAL> "<<="                           { return LT_LT_EQ; }
<YYINITIAL> "|"                             { return OR; }
<YYINITIAL> "|="                            { return OR_EQ; }
<YYINITIAL> "||"                            { return OR_OR; }
<YYINITIAL> "||="                           { return OR_OR_EQ; }
<YYINITIAL> "^"                             { return XOR; }
<YYINITIAL> "^="                            { return XOR_EQ; }
<YYINITIAL> "&"                             { return AND; }
<YYINITIAL> "&="                            { return AND_EQ; }
<YYINITIAL> "&&"                            { return AND_AND; }
<YYINITIAL> "&&="                           { return AND_AND_EQ; }
<YYINITIAL> "@"                             { return AT; }
<YYINITIAL> "#"                             { return HASH; }

<YYINITIAL> {NUMERIC_LITERAL} { return NUMBER; }

<YYINITIAL> {SINGLE_QUOTED_STRING} { return SINGLE_QUOTED_STRING; }
<YYINITIAL> {DOUBLE_QUOTED_STRING} { return DOUBLE_QUOTED_STRING; }

<YYINITIAL> [^] { return BAD_CHARACTER; }

package dev.kierat.drools.psi;

import com.intellij.ide.highlighter.custom.CustomFileTypeLexer;
import com.intellij.lexer.Lexer;
import com.intellij.lexer.LexerPosition;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DRLSimpleLexer extends Lexer {

    private final CustomFileTypeLexer lexer;

    public DRLSimpleLexer() {
        lexer = new CustomFileTypeLexer(DRLFileType.INSTANCE.getSyntaxTable());
    }

    @Override
    public void start(@NotNull CharSequence buffer, int startOffset, int endOffset, int initialState) {
        lexer.start(buffer, startOffset, endOffset, initialState);
    }

    @Override
    public int getState() {
        return lexer.getState();
    }

    @Override
    public @Nullable IElementType getTokenType() {
        return lexer.getTokenType();
    }

    @Override
    public int getTokenStart() {
        return lexer.getTokenStart();
    }

    @Override
    public int getTokenEnd() {
        return lexer.getTokenEnd();
    }

    @Override
    public void advance() {
        lexer.advance();
    }

    @Override
    public @NotNull LexerPosition getCurrentPosition() {
        return lexer.getCurrentPosition();
    }

    @Override
    public void restore(@NotNull LexerPosition position) {
        lexer.restore(position);
    }

    @Override
    public @NotNull CharSequence getBufferSequence() {
        return lexer.getBufferSequence();
    }

    @Override
    public int getBufferEnd() {
        return lexer.getBufferEnd();
    }

}
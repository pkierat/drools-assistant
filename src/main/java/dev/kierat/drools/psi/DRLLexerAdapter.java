package dev.kierat.drools.psi;

import com.intellij.lexer.FlexAdapter;

public class DRLLexerAdapter extends FlexAdapter {

    public DRLLexerAdapter() {
        super(new DRLLexer(null));
    }

}

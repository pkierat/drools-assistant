package dev.kierat.drools.psi;

import com.intellij.lang.ASTNode;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.fileTypes.PlainTextParserDefinition;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import com.intellij.psi.util.PsiUtilCore;
import dev.kierat.drools.psi.element.DRLElement;
import dev.kierat.drools.psi.element.DRLPackage;
import org.jetbrains.annotations.NotNull;


public class DRLParserDefinition implements ParserDefinition {

    private final PlainTextParserDefinition parserDefinition = new PlainTextParserDefinition();

    @Override
    @NotNull
    public Lexer createLexer(Project project) {
        return new DRLLexerAdapter();
    }

    @Override
    public @NotNull IFileElementType getFileNodeType() {
        return DRLElementTypes.DRL_FILE;
    }

    @Override
    public @NotNull PsiParser createParser(Project project) {
        return new DRLParser();
    }

    @Override
    public @NotNull TokenSet getCommentTokens() {
        return TokenSet.create(DRLTokenTypes.SINGLE_LINE_COMMENT);
    }

    @Override
    public @NotNull TokenSet getStringLiteralElements() {
        return TokenSet.create(DRLTokenTypes.DOUBLE_QUOTED_STRING);
    }

    @Override
    public @NotNull PsiElement createElement(ASTNode node) {
        if (node.getElementType() == DRLElementTypes.PACKAGE) return new DRLPackage(node);
        return new DRLElement(node);
    }

    @Override
    public @NotNull PsiFile createFile(@NotNull FileViewProvider viewProvider) {
        return new DRLFile(viewProvider);
    }

}

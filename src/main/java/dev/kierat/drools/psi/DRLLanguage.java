package dev.kierat.drools.psi;

import com.intellij.lang.Language;
import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public class DRLLanguage extends Language {

    public static DRLLanguage INSTANCE = new DRLLanguage();

    private DRLLanguage() {
        super("DRL");
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "DRL";
    }

    @Override
    public @Nullable LanguageFileType getAssociatedFileType() {
        return DRLFileType.INSTANCE;
    }
}

package dev.kierat.drools;

import com.intellij.ide.highlighter.custom.CustomFileHighlighter;
import dev.kierat.drools.psi.DRLFileType;

public class DRLSyntaxHighlighter extends CustomFileHighlighter {

    public DRLSyntaxHighlighter(DRLFileType fileType) {
        super(fileType.getSyntaxTable());
    }

}

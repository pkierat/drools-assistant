package dev.kierat.drools;

import com.intellij.openapi.fileTypes.PlainSyntaxHighlighter;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import dev.kierat.drools.psi.DRLFileType;
import org.jetbrains.annotations.NotNull;

public class DRLSyntaxHighlighterFactory extends SyntaxHighlighterFactory {
  @Override
  @NotNull
  public SyntaxHighlighter getSyntaxHighlighter(@NotNull Project project, @NotNull VirtualFile virtualFile) {
    if (!(virtualFile.getFileType() instanceof DRLFileType)) {
      return new PlainSyntaxHighlighter();
    }
    return new DRLSyntaxHighlighter((DRLFileType) virtualFile.getFileType());
  }
}
